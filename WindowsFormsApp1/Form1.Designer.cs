﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.buttonColor = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonPen = new System.Windows.Forms.PictureBox();
            this.buttonText = new System.Windows.Forms.PictureBox();
            this.buttonLines = new System.Windows.Forms.PictureBox();
            this.buttonBrush = new System.Windows.Forms.PictureBox();
            this.buttonEraser = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.createToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.authorsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonText)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonLines)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBrush)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEraser)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(690, 381);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pictureBox1_MouseMove);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Transparent;
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.buttonColor);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.buttonPen);
            this.panel1.Controls.Add(this.buttonText);
            this.panel1.Controls.Add(this.buttonLines);
            this.panel1.Controls.Add(this.buttonBrush);
            this.panel1.Controls.Add(this.buttonEraser);
            this.panel1.Controls.Add(this.pictureBox7);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 313);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(690, 68);
            this.panel1.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Location = new System.Drawing.Point(220, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 16);
            this.label3.TabIndex = 10;
            this.label3.Text = "Текущий цвет:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(402, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(72, 13);
            this.label2.TabIndex = 9;
            this.label2.Text = "Координаты:";
            // 
            // buttonColor
            // 
            this.buttonColor.Location = new System.Drawing.Point(220, 34);
            this.buttonColor.Name = "buttonColor";
            this.buttonColor.Size = new System.Drawing.Size(131, 23);
            this.buttonColor.TabIndex = 8;
            this.buttonColor.Text = "Выбрать цвет";
            this.buttonColor.UseVisualStyleBackColor = true;
            this.buttonColor.Click += new System.EventHandler(this.buttonColor_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Текущий инструмент:";
            // 
            // buttonPen
            // 
            this.buttonPen.BackColor = System.Drawing.Color.Transparent;
            this.buttonPen.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonPen.Image = global::WindowsFormsApp1.Properties.Resources.pencil;
            this.buttonPen.Location = new System.Drawing.Point(124, 34);
            this.buttonPen.Name = "buttonPen";
            this.buttonPen.Size = new System.Drawing.Size(22, 22);
            this.buttonPen.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.buttonPen.TabIndex = 6;
            this.buttonPen.TabStop = false;
            this.buttonPen.Tag = "Карандаш";
            this.buttonPen.MouseClick += new System.Windows.Forms.MouseEventHandler(this.objectClick);
            // 
            // buttonText
            // 
            this.buttonText.BackColor = System.Drawing.Color.Transparent;
            this.buttonText.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonText.Image = global::WindowsFormsApp1.Properties.Resources.text;
            this.buttonText.Location = new System.Drawing.Point(12, 34);
            this.buttonText.Name = "buttonText";
            this.buttonText.Size = new System.Drawing.Size(22, 22);
            this.buttonText.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.buttonText.TabIndex = 0;
            this.buttonText.TabStop = false;
            this.buttonText.Tag = "Текст";
            this.buttonText.MouseClick += new System.Windows.Forms.MouseEventHandler(this.objectClick);
            // 
            // buttonLines
            // 
            this.buttonLines.BackColor = System.Drawing.Color.Transparent;
            this.buttonLines.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonLines.Image = global::WindowsFormsApp1.Properties.Resources.line_curve;
            this.buttonLines.Location = new System.Drawing.Point(96, 34);
            this.buttonLines.Name = "buttonLines";
            this.buttonLines.Size = new System.Drawing.Size(22, 22);
            this.buttonLines.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.buttonLines.TabIndex = 5;
            this.buttonLines.TabStop = false;
            this.buttonLines.Tag = "Линия";
            this.buttonLines.MouseClick += new System.Windows.Forms.MouseEventHandler(this.objectClick);
            // 
            // buttonBrush
            // 
            this.buttonBrush.BackColor = System.Drawing.Color.Transparent;
            this.buttonBrush.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonBrush.Image = global::WindowsFormsApp1.Properties.Resources.brush;
            this.buttonBrush.InitialImage = null;
            this.buttonBrush.Location = new System.Drawing.Point(40, 34);
            this.buttonBrush.Name = "buttonBrush";
            this.buttonBrush.Size = new System.Drawing.Size(22, 22);
            this.buttonBrush.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.buttonBrush.TabIndex = 3;
            this.buttonBrush.TabStop = false;
            this.buttonBrush.Tag = "Кисть";
            this.buttonBrush.MouseClick += new System.Windows.Forms.MouseEventHandler(this.objectClick);
            // 
            // buttonEraser
            // 
            this.buttonEraser.BackColor = System.Drawing.Color.Transparent;
            this.buttonEraser.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.buttonEraser.Image = global::WindowsFormsApp1.Properties.Resources.eraser;
            this.buttonEraser.Location = new System.Drawing.Point(68, 34);
            this.buttonEraser.Name = "buttonEraser";
            this.buttonEraser.Size = new System.Drawing.Size(22, 22);
            this.buttonEraser.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.buttonEraser.TabIndex = 4;
            this.buttonEraser.TabStop = false;
            this.buttonEraser.Tag = "Ластик";
            this.buttonEraser.MouseClick += new System.Windows.Forms.MouseEventHandler(this.objectClick);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox7.Location = new System.Drawing.Point(309, 15);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(42, 16);
            this.pictureBox7.TabIndex = 11;
            this.pictureBox7.TabStop = false;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.aboutToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(690, 24);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.createToolStripMenuItem,
            this.openToolStripMenuItem,
            this.saveToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // createToolStripMenuItem
            // 
            this.createToolStripMenuItem.Name = "createToolStripMenuItem";
            this.createToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.createToolStripMenuItem.Text = "Create";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.openToolStripMenuItem.Text = "Open";
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.saveToolStripMenuItem.Text = "Save";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(108, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorsToolStripMenuItem});
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(52, 20);
            this.aboutToolStripMenuItem.Text = "About";
            // 
            // authorsToolStripMenuItem
            // 
            this.authorsToolStripMenuItem.Name = "authorsToolStripMenuItem";
            this.authorsToolStripMenuItem.Size = new System.Drawing.Size(116, 22);
            this.authorsToolStripMenuItem.Text = "Authors";
            // 
            // colorDialog1
            // 
            this.colorDialog1.FullOpen = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(690, 381);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pictureBox1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.buttonPen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonText)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonLines)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonBrush)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.buttonEraser)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox buttonText;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem createToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem authorsToolStripMenuItem;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button buttonColor;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox buttonPen;
        private System.Windows.Forms.PictureBox buttonLines;
        private System.Windows.Forms.PictureBox buttonBrush;
        private System.Windows.Forms.PictureBox buttonEraser;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.ColorDialog colorDialog1;
    }
}

