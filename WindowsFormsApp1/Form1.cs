﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public int x, y;
        public string instrument;
        public Color color;
        public Pen pen;
        PictureBox[] buttons;

        private void Form1_Load(object sender, EventArgs e)
        {
            color = Color.Black;
            pen = new Pen(color, 1);
            pictureBox7.BackColor = color;
        }

        public Form1()
        {
            InitializeComponent();
            buttons = new PictureBox[]
            {
                buttonBrush, buttonEraser, buttonText, buttonPen, buttonLines
            };
        }

        private void buttonColor_Click(object sender, EventArgs e)
        {
            colorDialog1.AllowFullOpen = false;
            colorDialog1.ShowHelp = true;
            colorDialog1.Color = color;

            if (colorDialog1.ShowDialog() == DialogResult.OK)
            {
                color = colorDialog1.Color;
                pictureBox7.BackColor = color;
            }
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            label2.Text = "{0} " + e.X + "{1}"+ e.Y;
        }

        void objectClick(object sender, MouseEventArgs e)
        {
            var b = sender as PictureBox;
            instrument = Convert.ToString(b.Tag);
            label1.Text = "Текущий инструмент: " + instrument;
        }
    }
}
